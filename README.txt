This module provides the active theme information in a
block, so users are not limited by their theme in their placement, but
can display them in any block region.

A css file is included, which can be used to tweak the display.
